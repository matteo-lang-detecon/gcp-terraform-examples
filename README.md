# GCP Terraform Examples

Here are a few examples of using Terraform on Google Cloud Platform.
They are adapted from the Google Cloud Skills Boost quest _Automating Infrastructure on Google Cloud with Terraform_.

## Usage
Go to [https://console.cloud.google.com/](https://console.cloud.google.com/), log in and select the project you were assigned to. Now you can open the Google Cloud Shell:

![Opening Google Cloud Shell](pics/open_cloud_shell.png "Open Google Cloud Shell")

In the Google Cloud Shell type the following lines. Lines preceeded with a `#` are comments.
```bash
# get your project id
gcloud config list project

# clone GitLab project to your Cloud Shell VM (TODO: change URL once we have our own instance)
git clone https://gitlab.com/matteo-lang-detecon/gcp-terraform-examples
```
Now open the Google Cloud editor:

![Opening Google Cloud Editor](pics/open_cloud_editor.png "Open Google Cloud Editor")
There you will see the folder `gcp-terraform-examples`. Open it and go into the subfolder `terraform`. Here you will need to rename `settings.auto.tfvars.TEMPLATE` to `settings.auto.tfvars`.
Open the file and fill in the project id you got with your first command and your name in the pattern of `firstname-lastname`. The changes will be saved automatically.

Now going back to the terminal, you can start executing terraform:
```bash
# go into the terraform folder
cd gcp-terraform-examples/terraform/

# initialize terraform
terraform init

# see what changes terraform wants to make, without actually changing anything
terraform plan

# create the resources
terraform apply
```
Type `yes` to start provisioning.<br>
<br>

Look around the code in the editor and browse the console to explore what you just created. The created ressources will all contain your name.<br>
<br>

<span style="color:red">**Important!**</span>  When you're done, don't forget to delete the resources:
```bash
terraform destroy
```

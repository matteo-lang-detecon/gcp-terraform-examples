###### provider definition: #################################################################################
terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
    }
  }
}
provider "google" {
  project     = var.gcp_project_id
  region      = var.gcp_region
  zone        = var.gcp_zone
}

####### network, firewall, static IPs: ######################################################################
resource "google_compute_network" "vpc_network" {
  name = "${var.resource-owner}-terraform-network"
}
module "firewall_rules" {
  source       = "terraform-google-modules/network/google//modules/firewall-rules"
  project_id   = var.gcp_project_id
  network_name = google_compute_network.vpc_network.name

  rules = [{
    name                    = "allow-ssh-ingress"
    description             = "Allow SSH to VMs in the network"
    direction               = "INGRESS"
    priority                = 1000
    ranges                  = ["0.0.0.0/0"]
    source_tags             = null
    source_service_accounts = null
    target_tags             = ["ssh"]
    target_service_accounts = null
    allow = [{
      protocol = "tcp"
      ports    = ["22"]
    }]
    deny = []
    log_config = {
      metadata = "INCLUDE_ALL_METADATA"
    }
  }]
}
resource "google_compute_address" "vm_static_ip" {
  name = "${var.resource-owner}-terraform-static-ip"
}

####### VMs: ################################################################################################
resource "google_compute_instance" "vm_instance_1" {
  name         = "${var.resource-owner}-terraform-instance-1"
  machine_type = "f1-micro"
  tags         = ["ssh"]
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }
    network_interface {
    network = google_compute_network.vpc_network.self_link
    access_config {
      nat_ip = google_compute_address.vm_static_ip.address
    }
  }
  provisioner "local-exec" { # writes the first VMs name and IP-address in a txt in the directory from which you ran terraform
    command = "echo ${google_compute_instance.vm_instance_1.name}:  ${google_compute_instance.vm_instance_1.network_interface[0].access_config[0].nat_ip} >> ip_address.txt"
  }
}
resource "google_compute_instance" "vm_instance_2" {
  # Tells Terraform that this VM instance must be created only after the
  # storage bucket has been created:
  depends_on   = [google_storage_bucket.example_bucket]
  name         = "${var.resource-owner}-terraform-instance-2"
  machine_type = "f1-micro"
  tags         = ["ssh"]
  boot_disk {
    initialize_params {
      image = "cos-cloud/cos-stable"
    }
  }
  network_interface {
    network = google_compute_network.vpc_network.self_link
    access_config {
    }
  }
}

###### storage ##############################################################################################
resource "google_storage_bucket" "example_bucket" {
  name     = "${var.resource-owner}-${formatdate("DD-MM-YYYY-hh-mm-ss", timestamp())}"
  location = "EU"
  website {
    main_page_suffix = "index.html"
    not_found_page   = "404.html"
  }
}
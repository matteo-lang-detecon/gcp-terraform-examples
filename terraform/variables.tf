variable "gcp_project_id" {
  type        = string
  description = "GCP project ID - get with `gcloud config list project`"
  nullable    = false
}
variable "gcp_region" {
  type        = string
  description = "GCP region to be used"
  default     = "europe-west1"
  nullable    = false
}
variable "gcp_zone" {
  type        = string
  description = "GCP zone to be used"
  default     = "europe-west1-b"
  nullable    = false
}
variable "resource-owner" { # with this variable, you can see how variable validation could be implemented
  type        = string
  description = "Name of resource creator to identify. Must only contain letters, numbers and hyphens"
  validation {
    condition     = length(var.resource-owner) >= 3 && length(var.resource-owner) <= 41 # length is limited, so derived resource names are valid
    error_message = "The resource-owner variable must be 3-42 characters in length."
  }
  validation { # This condition could also be used to validate whole VPC names
    condition     = can(regex("^([a-z]([a-z0-9-]{0,61}[a-z0-9])?.)*[a-z]([a-z0-9-]{0,61}[a-z0-9])?$", var.resource-owner))
    error_message = "The resource-owner variable must only contain lowercase letters, numbers or hyphens, must start with a letter and cannot end with a hyphen."
  }
  nullable    = false
}